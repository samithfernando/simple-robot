export const teleportRobot = coordinates => {
  return {
    type: 'TELEPORT_ROBOT',
    payload: coordinates
  };
};

export const moveLeft = () => {
  return {
    type: 'MOVE_LEFT',
    payload: -1
  };
};

export const moveRight = () => {
  return {
    type: 'MOVE_RIGHT',
    payload: 1
  };
};

export const moveUp = () => {
  return {
    type: 'MOVE_UP',
    payload: 1
  };
};

export const moveDown = () => {
  return {
    type: 'MOVE_DOWN',
    payload: -1
  };
};
