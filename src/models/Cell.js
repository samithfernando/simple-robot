export default class Cell {
  constructor(x, y) {
    this.xCoordinate = x;
    this.yCoordinate = y;
  }
}
