import { robotPositionReducer } from '../reducers/index';

describe('Robot', () => {
  it('should be at its initial state when no state is defined', () => {
    expect(robotPositionReducer(undefined, {})).toEqual({
      x: 1,
      y: 1
    });
  });

  it('should not move left from its initial state', () => {
    expect(
      robotPositionReducer(
        { x: 1, y: 1 },
        {
          type: 'MOVE_LEFT',
          payload: -1
        }
      )
    ).toEqual({
      x: 1,
      y: 1
    });
  });

  it('should not move down from its initial state', () => {
    expect(
      robotPositionReducer(
        { x: 1, y: 1 },
        {
          type: 'MOVE_DOWN',
          payload: -1
        }
      )
    ).toEqual({
      x: 1,
      y: 1
    });
  });

  it('should not move beyond from its x coordinates', () => {
    expect(
      robotPositionReducer(
        { x: 5, y: 1 },
        {
          type: 'MOVE_RIGHT',
          payload: 1
        }
      )
    ).toEqual({
      x: 5,
      y: 1
    });
  });

  it('should not move beyond from its y coordinates', () => {
    expect(
      robotPositionReducer(
        { x: 1, y: 5 },
        {
          type: 'MOVE_UP',
          payload: 1
        }
      )
    ).toEqual({
      x: 1,
      y: 5
    });
  });
});
