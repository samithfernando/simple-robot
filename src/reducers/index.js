import { combineReducers } from 'redux';
import _ from 'lodash';

import Cell from '../models/Cell';

const gridReducer = () => {
  return initGrid();
};

export const robotPositionReducer = (
  robotPosition = { x: 1, y: 1 },
  action
) => {
  const { x, y } = robotPosition;
  switch (action.type) {
    case 'MOVE_LEFT':
      if (x + action.payload >= 1) {
        robotPosition.x = x + action.payload;
      }
      break;
    case 'MOVE_RIGHT':
      if (x + action.payload <= 5) {
        robotPosition.x = x + action.payload;
      }
      break;
    case 'MOVE_UP':
      if (y + action.payload <= 5) {
        robotPosition.y = y + action.payload;
      }
      break;
    case 'MOVE_DOWN':
      if (y + action.payload >= 1) {
        robotPosition.y = y + action.payload;
      }
      break;
    case 'TELEPORT_ROBOT':
      robotPosition = action.payload;
      break;

    default:
      break;
  }

  return robotPosition;
};

/**
 * dynmically created 'Cell' model array for initial usage,
 * the rendered grid is an array of arrays:
 *  0: (5) [Cell, Cell, Cell, Cell, Cell] => x1 to y5
 *  1: (5) [Cell, Cell, Cell, Cell, Cell] => x2 to y5
 *  2: (5) [Cell, Cell, Cell, Cell, Cell] => x3 to y5
 *  3: (5) [Cell, Cell, Cell, Cell, Cell] => x4 to y5
 *  4: (5) [Cell, Cell, Cell, Cell, Cell] => x5 to y5
 */
const initGrid = () => {
  const grid = [];
  for (let x = 1; x <= 5; x++) {
    for (let y = 1; y <= 5; y++) {
      grid.push(new Cell(x, y));
    }
  }
  return _.chunk(grid, 5);
};

export default combineReducers({
  grid: gridReducer,
  robotPosition: robotPositionReducer
});
