import React from 'react';
import { connect } from 'react-redux';

import {
  moveLeft,
  moveRight,
  moveUp,
  moveDown,
  teleportRobot
} from '../actions';

class Commander extends React.Component {
  render() {
    return (
      <div>
        <div className="field has-addons">
          <p className="control">
            <button
              className="button"
              id="btn-left"
              onClick={this.props.moveLeft}
            >
              <span className="icon is-small">
                <i className="fa fa-arrow-left" />
              </span>
              <span>Left</span>
            </button>
          </p>
          <p className="control">
            <button
              className="button"
              id="btn-right"
              onClick={this.props.moveRight}
            >
              <span className="icon is-small">
                <i className="fa fa-arrow-right" />
              </span>
              <span>Right</span>
            </button>
          </p>
          <p className="control">
            <button className="button" id="btn-up" onClick={this.props.moveUp}>
              <span className="icon is-small">
                <i className="fa fa-arrow-up" />
              </span>
              <span>Up</span>
            </button>
          </p>
          <p className="control">
            <button
              className="button"
              id="btn-down"
              onClick={this.props.moveDown}
            >
              <span className="icon is-small">
                <i className="fa fa-arrow-down" />
              </span>
              <span>Down</span>
            </button>
          </p>
        </div>
      </div>
    );
  }

  // initiate first teleport into {1, 1}
  componentDidMount() {
    this.props.teleportRobot({ x: 1, y: 1 });
  }
}

const mapStateToProps = state => {
  return { robotPosition: state.robotPosition };
};

export default connect(
  mapStateToProps,
  { moveLeft, moveRight, moveUp, moveDown, teleportRobot }
)(Commander);
