import React from 'react';

import CellGrid from './CellGrid';
import Commander from './Commander';

const App = () => {
  return (
    <div>
      <div className="columns is-centered">
        <div className="column is-6 is-offset-3">
          <CellGrid />
          <br />
          <Commander />
        </div>
      </div>
    </div>
  );
};

export default App;
