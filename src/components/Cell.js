import React from 'react';

import { connect } from 'react-redux';
import './Cell.css';

class Cell extends React.Component {
  render() {
    return (
      <div
        className={'cell' + (this.isRobotPlaced() ? ' robot' : '')}
        onClick={() =>
          this.props.teleport({
            x: this.props.cell.xCoordinate,
            y: this.props.cell.yCoordinate
          })
        }
      />
    );
  }

  /**
   * check whether the selected cell has the robot current position
   */
  isRobotPlaced() {
    if (
      this.props.robotPosition.x === this.props.cell.xCoordinate &&
      this.props.robotPosition.y === this.props.cell.yCoordinate
    ) {
      return true;
    }
    return false;
  }
}

const mapStateToProps = state => {
  return { robotPosition: state.robotPosition };
};

export default connect(mapStateToProps)(Cell);
