import React from 'react';
import { connect } from 'react-redux';

import Cell from './Cell';
import { teleportRobot } from '../actions/index';
import './CellGrid.css';

class CellGrid extends React.Component {
  render() {
    // initiate the grid system
    return this.renderGrid();
  }

  /**
   * rendering grid by 5 rows
   */
  renderGrid() {
    return (
      <div className="cell-grid">
        {this.props.grid.map((gridRow, idx) => {
          return this.renderRow(gridRow, idx);
        })}
      </div>
    );
  }

  /**
   * rendering each 'row' by 5 'Cell' columns
   */
  renderRow(gridRow, idx) {
    return (
      <div className="raw-wrapper" key={idx}>
        {gridRow.map(gridColumn => {
          return this.renderColumn(gridColumn);
        })}
      </div>
    );
  }

  /**
   * finally render 'Cell' column
   */
  renderColumn(gridColumn) {
    return (
      <Cell
        cell={gridColumn}
        teleport={this.props.teleportRobot}
        key={gridColumn.xCoordinate + '' + gridColumn.yCoordinate}
      />
    );
  }
}

const mapStateToProps = state => {
  return state;
};

export default connect(
  mapStateToProps,
  { teleportRobot }
)(CellGrid);
