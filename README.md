This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Simple-Robot App

In the project directory, you can run:

### `Demo`

you can find a working example [here](https://simple-robot-7564b.firebaseapp.com/)

### `npm start`

to run project locally run '`npm start`'

### `npm test`

to test project, run '`npm test`'

### `npm run build`

to build project, run '`npm run build`'
